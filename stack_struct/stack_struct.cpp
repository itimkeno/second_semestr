#include <iostream>
#include <random>

// declare the structure template, the type of the stack element
struct stack_not_std
{
    int data;
    // stores the location address of the underlying element
    stack_not_std* next;
};

stack_not_std* push(stack_not_std*, int);
stack_not_std* push_stdin(stack_not_std*);
stack_not_std* pop(stack_not_std*);
stack_not_std* split_by_even_and_odd(stack_not_std**);
void           print_all(stack_not_std*);
void           clear(stack_not_std**);
void           fill_stack_random_numbers(stack_not_std**, size_t count);
bool           stack_sort_ascending(stack_not_std**);

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    std::cout << "Vaboratory work 3. Variant 12." << std::endl;

    stack_not_std* primeval_stack = nullptr;

    fill_stack_random_numbers(&primeval_stack, 20);

    std::cout << "----- Source stack -----\n";
    print_all(primeval_stack);

    bool is_sorted = stack_sort_ascending(&primeval_stack);
    if (is_sorted)
    {
        std::cout << "----- Stack after ascending sort -----\n";
        print_all(primeval_stack);
    }

    stack_not_std* even_stack = split_by_even_and_odd(&primeval_stack);

    std::cout << "----- Stack with even numbers -----\n";
    print_all(even_stack);
    std::cout << "----- Stack with odd numbers -----\n";
    print_all(primeval_stack);

    clear(&primeval_stack);
    clear(&even_stack);

    return is_sorted;
}

void fill_stack_random_numbers(stack_not_std** head, size_t count)
{
    std::random_device              rand_device;
    std::mt19937                    generator(rand_device());
    std::uniform_int_distribution<> distrib(-100, 100);

    for (size_t i = 0; i < count; ++i)
        *head = push(*head, distrib(generator));
}

stack_not_std* push(stack_not_std* head, int num)
{
    stack_not_std* new_head = new stack_not_std;
    new_head->data          = num;
    new_head->next          = head;

    return new_head;
}

stack_not_std* push_stdin(stack_not_std* head)
{
    stack_not_std* new_head = new stack_not_std;
    std::cout << "Enter integer number: ";
    std::cin >> new_head->data;
    new_head->next = head;

    return new_head;
}

stack_not_std* pop(stack_not_std* head)
{
    stack_not_std* tmp = head->next;
    delete head;
    return tmp;
}

// print all data from stack
void print_all(stack_not_std* head)
{
    while (head != nullptr)
    {
        std::cout << head->data << std::endl;
        head = head->next;
    }
}
// delete all nodes from stack
void clear(stack_not_std** head)
{
    stack_not_std* tmp;
    while (*head != nullptr)
    {
        tmp   = *head;
        *head = (*head)->next;
        delete tmp;
    }
}

// [result] result pointer - to stack with even elements,
// [in,out] parameter pointer - to stack with odd elements
stack_not_std* split_by_even_and_odd(stack_not_std** head)
{
    stack_not_std* top      = *head;
    stack_not_std* top_even = nullptr;
    stack_not_std* top_odd  = nullptr;

    while (top != nullptr)
    {
        if (top->data % 2 == 0)
            top_even = push(top_even, top->data);
        else
        {
            // top_odd = push(top_odd, top->data);
            stack_not_std* tmp = new stack_not_std;
            tmp->data          = top->data;
            tmp->next          = top_odd;
            top_odd            = tmp;
        }

        top = top->next;
    }

    clear(&*head);

    *head = top_odd;

    return top_even;
}

bool stack_sort_ascending(stack_not_std** head)
{
    if (*(head) == nullptr || (*head)->next == nullptr ||
        (*head)->next->next == nullptr)
        return false;

    stack_not_std* tmp_head = nullptr;
    stack_not_std* guard    = nullptr;
    stack_not_std* transfer = nullptr;

    *head = push(*head, INT_MAX);

    do
    {
        for (tmp_head = *head; tmp_head->next->next != guard;
             tmp_head = tmp_head->next)
        {

            if (tmp_head->next->data > tmp_head->next->next->data)
            {
                transfer             = tmp_head->next->next;
                tmp_head->next->next = transfer->next;
                transfer->next       = tmp_head->next;
                tmp_head->next       = transfer;
            }
        }
        guard = tmp_head->next;
    } while ((*head)->next->next != guard);

    *head = pop(*head);
    return true;
}
