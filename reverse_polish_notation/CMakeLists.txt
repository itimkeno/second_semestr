﻿cmake_minimum_required (VERSION 3.18)

add_executable(reverse_polish_notation "reverse_polish_notation.cpp")
target_compile_features(reverse_polish_notation PRIVATE cxx_std_20)

if(MSVC)
  target_compile_options(reverse_polish_notation PRIVATE /W4 /WX /std:c++20)
else()
  target_compile_options(reverse_polish_notation PRIVATE -Wall -Wextra -Wpedantic -Werror)
endif()