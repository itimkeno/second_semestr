#include <iostream>

static const size_t EXPRESSION_BUFFER = 64;

struct stack_for_chars
{
    char             symbol;
    stack_for_chars* previous;
};
stack_for_chars* push_in_stack(stack_for_chars*, char);
stack_for_chars* pop_out_stack(stack_for_chars*, char*);
stack_for_chars* pop_out_stack(stack_for_chars*);

// transformation expression from regular view to to reverse polish notation
// input_string - regular expression
// output_string - reverse polish notation expression
void   transform_str_to_rpn(const char* input_string, char* output_string);
int    priority(const char);
double calculate_rpn_expression(const char* rpn_string, const char* variables,
                                const double* variables_values);

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    std::cout << "Vaboratory work 5. Variant 11." << std::endl;

    char         input_string[EXPRESSION_BUFFER];
    char         output_string[EXPRESSION_BUFFER];
    const size_t number_of_variables      = 6;
    char   variables[number_of_variables] = { 'a', 'b', 'c', 'd', 'e', '\0' };
    double variables_values[number_of_variables - 1] = { 0.0 };

    std::cout << "Please, enter source expression: ";
    std::cin >> input_string;

    std::cout << "Now, fill in the variables values: " << std::endl;
    for (size_t i = 0; i < strlen(variables); ++i)
    {
        std::cout << variables[i] << " : ";
        std::cin >> variables_values[i];

        if (std::cin.fail())
        {
            std::cerr << "Wrong input. Program was corrupted.";
            std::exit(EXIT_FAILURE);
        }
    }
    std::cout << std::endl;

    transform_str_to_rpn(input_string, output_string);

    std::cout << "Reverse polish notation: " << output_string << '\n';

    double result =
        calculate_rpn_expression(output_string, variables, variables_values);

    std::cout << "Answer: " << result << std::endl;

    return EXIT_SUCCESS;
}

int priority(const char operation)
{
    switch (operation)
    {
        case '*':
        case '/':
            return 3;
        case '+':
        case '-':
            return 2;
        case '(':
            return 1;
    }
    return 0;
}

void transform_str_to_rpn(const char* input_string, char* output_string)
{
    stack_for_chars* operators = nullptr;
    char             symbol;

    size_t input_position  = 0;
    size_t output_position = 0;

    for (; input_position < EXPRESSION_BUFFER; ++input_position)
    {
        if (input_string[input_position] == '\0')
            break;

        if (input_string[input_position] == ')')
        {
            while (operators->symbol != '(')
            {
                operators = pop_out_stack(operators, &symbol);
                if (!operators)
                    symbol = '\0';
                output_string[output_position++] = symbol;
            }
            operators = pop_out_stack(operators);
        }

        if (input_string[input_position] >= 'a' &&
            input_string[input_position] <= 'z')
        {
            output_string[output_position++] = input_string[input_position];
        }
        if (input_string[input_position] == '(')
        {
            operators = push_in_stack(operators, input_string[input_position]);
        }
        if (input_string[input_position] == '+' ||
            input_string[input_position] == '-' ||
            input_string[input_position] == '*' ||
            input_string[input_position] == '/')
        {
            while (operators != nullptr &&
                   priority(operators->symbol) >=
                       priority(input_string[input_position]))
            {
                operators = pop_out_stack(operators, &symbol);
                output_string[output_position++] = symbol;
            }
            operators = push_in_stack(operators, input_string[input_position]);
        }
    }

    while (operators != nullptr)
    {
        operators                        = pop_out_stack(operators, &symbol);
        output_string[output_position++] = symbol;
    }

    output_string[output_position] = '\0';
}

double calculate_rpn_expression(const char* rpn_string, const char* variables,
                                const double* variables_values)
{
    double intermediate_calculations[201] = { 0.0 };
    char   var_symbol;

    for (size_t i = 0; i < strlen(variables); ++i)
    {
        var_symbol                                 = variables[i];
        intermediate_calculations[int(var_symbol)] = variables_values[i];
    }

    stack_for_chars* expression = nullptr;

    char   chr = 'z' + 1;
    char   left_var, right_var;
    double result_value = 0.0;
    double left_value   = 0.0;
    double right_value  = 0.0;

    for (size_t i = 0; i < strlen(rpn_string); ++i)
    {
        if (rpn_string[i] != '+' && rpn_string[i] != '-' &&
            rpn_string[i] != '*' && rpn_string[i] != '/')
        {
            expression = push_in_stack(expression, rpn_string[i]);
        }
        else
        {
            expression  = pop_out_stack(expression, &left_var);
            expression  = pop_out_stack(expression, &right_var);
            left_value  = intermediate_calculations[int(left_var)];
            right_value = intermediate_calculations[int(right_var)];
            switch (rpn_string[i])
            {
                case '+':
                    result_value = right_value + left_value;
                    break;
                case '-':
                    result_value = right_value - left_value;
                    break;
                case '*':
                    result_value = right_value * left_value;
                    break;
                case '/':
                    result_value = right_value / left_value;
                    break;
                default:
                    std::cerr << "Program was corrupted.";
                    std::exit(EXIT_FAILURE);
            }
            intermediate_calculations[int(chr)] = result_value;
            expression = push_in_stack(expression, chr);
            chr++;
        }
    }
    return result_value;
}

stack_for_chars* push_in_stack(stack_for_chars* head, const char symbol)
{
    stack_for_chars* new_head = new stack_for_chars;
    new_head->symbol          = symbol;
    new_head->previous        = head;
    return new_head;
}

stack_for_chars* pop_out_stack(stack_for_chars* head, char* symbol)
{
    stack_for_chars* tmp = head;
    *symbol              = head->symbol;
    head                 = head->previous;
    delete tmp;
    return head;
}

stack_for_chars* pop_out_stack(stack_for_chars* head)
{
    stack_for_chars* tmp = head;
    head                 = head->previous;
    delete tmp;
    return head;
}