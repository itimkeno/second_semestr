﻿# CMakeList.txt : CMake project for recursive_algorithm, include source and define
# project specific logic here.
#
cmake_minimum_required (VERSION 3.18)

# Add source to this project's executable.
add_executable (recursive_algorithm "recursive_algorithm.cpp" )
#set_property(TARGET recursive_algorithm PROPERTY CXX_STANDART 20)

if(MSVC)
  target_compile_options(recursive_algorithm PRIVATE /W4 /WX /std:c++20)
else()
  target_compile_options(recursive_algorithm PRIVATE -Wall -Wextra -Wpedantic -Werror)
endif()

# TODO: Add tests and install targets if needed.
