﻿#include <iostream>

static constexpr const char* pre_requirements = "'n' must be greater than 'k'";

// some magic (optimization)
unsigned long long binomial_coefficient_recursive(unsigned long long n,
                                                  unsigned long long k)
{
    if (k > n)
        return 0;
    if (k > n - k)
        k = n - k;
    if (k == 0 || n <= 1)
        return 1;

    return binomial_coefficient_recursive(n - 1, k) +
           binomial_coefficient_recursive(n - 1, k - 1);
}

unsigned long long binomial_coefficient_linear(unsigned long long n,
                                               unsigned long long k)
{
    unsigned long long c = 1;

    if (k > n - k) // take advantage of symmetry
        k = n - k;

    for (unsigned long long i = 1; i <= k; i++, n--)
    {
        // check on potential overflow
        if (c / i >= ULONG_MAX / n)
            return 0;
        // split c * n / i into (c / i * i + c % i) * n / i
        c = c / i * n + c % i * n / i;
    }

    return c;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    int n = 0;
    int k = 0;

    std::cout << "Laboratory work 1. Variant 14." << std::endl;
    std::cout << "Please, enter two numbers ( " << pre_requirements
              << " ) :" << std::endl;
    std::cout << "n = ";
    std::cin >> n;
    std::cout << "k = ";
    std::cin >> k;

    if (n < 0 || k < 0)
    {
        std::cout << "incorrect input, n and k must be greater than zero."
                  << std::endl;
        return EXIT_FAILURE;
    }

    if (n < k)
    {
        std::cout << "incorrect input, " << pre_requirements << std::endl;
        std::cout << n << '<' << k << std::endl;

        return EXIT_FAILURE;
    }

    std::cout << "Recursive: " << binomial_coefficient_recursive(n, k)
              << std::endl;

    unsigned long long binom_result = binomial_coefficient_linear(n, k);
    if (binom_result == 0)
        std::cout << "Binomial coefficient was owerflowed." << std::endl;
    else
        std::cout << "Linear: " << binom_result << std::endl;

    return EXIT_SUCCESS;
}
