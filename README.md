## Second semester BSUIR FIK
# Laboratory works Variant 19

This project contains 4 lab works

Language: [C++]                                              
Build system: [CMake] (3.5 or higher) and [ninja] or [make]

You can open this project as a folder in any IDE with built-in CMake support.

[C++]: https://en.wikipedia.org/wiki/C%2B%2B
[CMake]: https://cmake.org/
[ninja]: https://ninja-build.org/
[make]: https://www.gnu.org/software/make/
