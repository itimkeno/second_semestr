﻿cmake_minimum_required (VERSION 3.18)

add_executable(search_algorithm "search_algorithm.cpp")
target_compile_features(search_algorithm PRIVATE cxx_std_20)

if(MSVC)
  target_compile_options(search_algorithm PRIVATE /W4 /WX /std:c++20)
else()
  target_compile_options(search_algorithm PRIVATE -Wall -Wextra -Wpedantic -Werror)
endif()