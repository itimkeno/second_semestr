﻿#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>

static const size_t DATE_WIDTH = 11;
static const size_t CITY_WIDTH = 25;
static const size_t TIME_WIDTH = 10;
static const size_t SEAT_WIDTH = 8;

enum class menu
{
    view_schedule           = 1,
    add_new_train_into_file = 2,
    linear_search           = 3,
    linear_sort             = 4,
    quickSort               = 5,
    binary_search           = 6,
    private_task            = 7,
    create_new_file         = 9,
    exit                    = 0
};

struct train
{
    train()  = default;
    ~train() = default;
    train(std::string departure_date, std::string destination_point,
          std::string departure_time, size_t seats)
        : departure_date_(departure_date)
        , destination_point_(destination_point)
        , departure_time_(departure_time)
        , available_seats_(seats)
    {
    }

    std::string departure_date_;
    std::string destination_point_;
    std::string departure_time_;
    size_t      available_seats_ = 0;
};

// helper for read fixing charachters sequence
std::string read_buff(std::ifstream& stream, const size_t count)
{
    std::string result(count, ' ');
    stream.read(&result[0], count);
    return result;
}

std::istream& operator>>(std::istream& in_file, train& train_data)
{
    in_file >> train_data.departure_date_;
    in_file >> train_data.destination_point_;
    in_file >> train_data.departure_time_;
    in_file >> train_data.available_seats_;

    return in_file;
}

std::ostream& operator<<(std::ostream& in_file, const train& train_data)
{
    in_file << std::setw(DATE_WIDTH) << train_data.departure_date_;
    in_file << std::setw(CITY_WIDTH) << train_data.destination_point_;
    in_file << std::setw(TIME_WIDTH) << train_data.departure_time_;
    in_file << std::setw(SEAT_WIDTH) << train_data.available_seats_;
    in_file << std::endl;

    return in_file;
}

train get_train_stdin()
{
    train user_train;

    std::cout
        << "Please, enter destination city, departure date, time and count "
           "of seats.\n";
    std::cout << "City: ";
    std::cin.clear();
    std::cin.sync(); // clear stdin buffer
    std::getline(std::cin >> std::ws, user_train.destination_point_);
    std::cout << "Departure date: ";
    std::cin >> user_train.departure_date_;
    std::cout << "Departure time: ";
    std::cin >> user_train.departure_time_;
    std::cout << "Count of seats: ";
    std::cin >> user_train.available_seats_;

    if (std::cin.bad() || std::cout.bad())
        return train();

    return user_train;
}

std::vector<train> read_schedule_from_file(const std::string_view path)
{
    std::vector<train> schedule;
    if (!std::filesystem::exists(path.data()))
    {
        std::cout
            << "File \"" << path
            << "\" doesn't exist. Please, create new file before adding trains."
            << std::endl;
        return schedule;
    }

    std::ifstream schedule_file(path.data());

    if (!schedule_file.is_open())
    {
        std::cerr << "File \"" << path << "\" coudn't be open." << std::endl;
        return schedule;
    }

    train train_data;
    while (schedule_file >> train_data)
        schedule.push_back(train_data);

    return schedule;
}

bool depature_before_requested_time(const std::string_view train_time,
                                    const std::string_view user_time)
{
    const size_t hours_in_one_day    = 23;
    const size_t minutes_in_one_hour = 59;

    auto train_hours   = train_time.substr(0, train_time.find(":"));
    auto train_minutes = train_time.substr(train_time.find(":") + 1);

    auto user_hours   = user_time.substr(0, user_time.find(":"));
    auto user_minutes = user_time.substr(user_time.find(":") + 1);

    size_t train_hours_num   = std::stoull(train_hours.data());
    size_t train_minutes_num = std::stoull(train_minutes.data());

    size_t user_hours_num = std::stoull(user_hours.data());
    if (user_hours_num > hours_in_one_day)
        return false;

    size_t user_minutes_num = std::stoull(user_minutes.data());
    if (user_minutes_num > minutes_in_one_hour)
        return false;

    if (user_hours_num <= train_hours_num &&
        user_minutes_num <= train_minutes_num)
        return false;

    return true;
}

bool ordering_tickets(const std::vector<train>& schedule)
{
    train requested_train = get_train_stdin();

    std::vector<std::string_view> avaliable_trains;
    for (auto& each_train : schedule)
    {
        if (each_train.destination_point_ !=
                requested_train.destination_point_ ||
            each_train.departure_date_ != requested_train.departure_date_ ||
            each_train.available_seats_ < requested_train.available_seats_ ||
            !depature_before_requested_time(each_train.departure_time_,
                                            requested_train.departure_time_))
            continue;

        avaliable_trains.push_back(each_train.departure_time_);
    }

    if (avaliable_trains.empty())
        return false;

    std::cout << "\nAvaliable trains on " << requested_train.departure_date_
              << " to " << requested_train.destination_point_ << " with "
              << requested_train.available_seats_
              << " free seats :" << std::endl;

    for (auto& each_train : avaliable_trains)
        std::cout << "Departure at " << each_train << std::endl;

    return true;
}

bool add_new_train_into_file(const train&           recordable_train,
                             const std::string_view path)
{
    if (!std::filesystem::exists(path.data()))
    {
        std::cout
            << "File \"" << path
            << "\" doesn't exist. Please, create new file before adding trains."
            << std::endl;
        return true;
    }

    std::ofstream schedule(path.data(), std::ios_base::app);
    if (schedule.bad())
        return false;

    schedule << recordable_train;

    if (schedule.bad())
        return false;

    return true;
}

bool view_schedule(const std::vector<train>& schedule)
{
    if (schedule.empty())
        return false;

    size_t seporator_len = DATE_WIDTH + CITY_WIDTH + TIME_WIDTH + SEAT_WIDTH;
    std::cout << std::setw(DATE_WIDTH) << "Departure date"
              << std::setw(CITY_WIDTH) << "Destination point"
              << std::setw(TIME_WIDTH) << "Time" << std::setw(SEAT_WIDTH)
              << "Seats" << std::endl;
    std::cout << std::string(seporator_len, '-') << std::endl;

    for (auto& each_train : schedule)
        std::cout << each_train;

    return true;
}

// find all occurance in file
void linear_search_into_file(const std::string_view path)
{
    if (!std::filesystem::exists(path.data()))
    {
        std::cout
            << "File \"" << path
            << "\" doesn't exist. Please, create new file before adding trains."
            << std::endl;
        return;
    }

    size_t seats;
    std::cout << "Please, enter count of seats (which are the key): ";
    std::cin >> seats;

    train         tr;
    std::ifstream schedule(path.data(), std::ios_base::beg);

    while (schedule >> tr)
        if (tr.available_seats_ == seats)
        {
            std::cout << tr;
            return;
        }

    std::cout << "Requested count of seat not found." << std::endl;
}

bool linear_sort_schedule_in_file(const std::string_view path)
{
    std::vector<train> trains = read_schedule_from_file(path);
    if (trains.empty())
        return false;

    // linear sort with replacing
    for (size_t i = 0; i < trains.size() - 1; ++i)
        for (size_t k = i + 1; k < trains.size(); ++k)
            if (trains[i].available_seats_ > trains[k].available_seats_)
            {
                train tmp = trains[i];
                trains[i] = trains[k];
                trains[k] = tmp;
            }
    // clear all data from schedule file
    std::ofstream rewrite_file(path.data());
    rewrite_file.clear();
    rewrite_file.close();

    if (rewrite_file.bad())
        return false;

    // rewrite sorted schedule
    for (auto& each_train : trains)
        if (false == add_new_train_into_file(each_train, path))
            return false;

    std::cout << "Schedule was sorted successfully." << std::endl;

    return true;
}

/* This function takes last element as pivot, places
the pivot element at its correct position in sorted
array, and places all smaller (smaller than pivot)
to left of pivot and all greater elements to right
of pivot */
size_t partition(std::vector<train>& arr, size_t low, size_t high)
{
    size_t pivot = arr[high].available_seats_; // pivot
    size_t i = (low - 1); // Index of smaller element and indicates the right
                          // position of pivot found so far

    for (size_t j = low; j <= high - 1; j++)
    {
        // If current element is smaller than the pivot
        if (arr[j].available_seats_ < pivot)
        {
            i++; // increment index of smaller element
            // swap(&arr[i], &arr[j]);
            train tmp = arr[i];
            arr[i]    = arr[j];
            arr[j]    = tmp;
        }
    }
    // swap(&arr[i + 1], &arr[high]);
    train tmp  = arr[i + 1];
    arr[i + 1] = arr[high];
    arr[high]  = tmp;
    return (i + 1);
}

/* The main function that implements QuickSort
arr[] --> Array to be sorted,
low --> Starting index,
high --> Ending index */
void quick_sort(std::vector<train>& arr, size_t low, size_t high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
        at right place */
        size_t pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quick_sort(arr, low, pi - 1);
        quick_sort(arr, pi + 1, high);
    }
}

bool quick_sort_schedule_in_file(const std::string_view path)
{
    std::vector<train> trains = read_schedule_from_file(path);
    if (trains.empty())
        return false;

    quick_sort(trains, 0, trains.size() - 1);

    // clear all data from schedule file
    std::ofstream rewrite_file(path.data());
    rewrite_file.clear();
    rewrite_file.close();

    if (rewrite_file.bad())
        return false;

    // rewrite sorted schedule
    for (auto& each_train : trains)
        if (false == add_new_train_into_file(each_train, path))
            return false;

    std::cout << "Schedule was sorted successfully." << std::endl;

    return true;
}

bool is_sorted_ascending(const std::vector<train>& schedule)
{
    for (size_t i = 0; i < schedule.size() - 1; ++i)
        if (schedule[i].available_seats_ > schedule[i + 1].available_seats_)
            return false;

    return true;
}

train binary_search_schedule(const std::vector<train>& schedule,
                             const size_t              seats)
{
    train tr;
    if (false == is_sorted_ascending(schedule))
        return tr;

    size_t left  = 0;
    size_t right = schedule.size() - 1;

    while (left <= right)
    {
        size_t middle = left + (right - left) / 2;

        // Check if "seats" is present at mid
        if (schedule[middle].available_seats_ == seats)
            return schedule[middle];

        // If "seats" greater, ignore left half
        if (schedule[middle].available_seats_ < seats)
            left = middle + 1;

        // If "seats" is smaller, ignore right half
        else
            right = middle - 1;
    }

    return tr;
}

bool create_new_schedule_file(const std::string_view path)
{
    if (std::filesystem::exists(path.data()))
    {
        std::cout << "File \"" << path << "\" already created." << std::endl;
        return true;
    }

    std::ofstream file_schedule(path.data(), std::ios_base::app);

    if (!file_schedule)
    {
        std::cerr << "File \"" << path << "\" could not be created.\n"
                  << "bla-bla-bla some errors checks..." << std::endl;
        return false;
    }

    std::cout << "File \"" << path << "\" created successfully." << std::endl;
    return true;
}

void show_menu()
{
    std::cout << "\n\tMenu"
                 "\n1.View schedule from file."
                 "\n2.Add new train into file."
                 "\n3.Linear search."
                 "\n4.Linear sort."
                 "\n5.QuickSort."
                 "\n6.Binary search in sorted data."
                 "\n7.Private task (13 variant)."
                 "\n9.Create new file."
                 "\n0.Exit."
              << std::endl;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char** argv)
{
    std::cout << "Laboratory work 2. Variant 13." << std::endl;

    constexpr const char* schedule_path = "schedule.txt";

    bool is_exit = false;
    while (false == is_exit)
    {
        show_menu();

        size_t menu_item = 0;
        std::cin >> menu_item;

        menu menu_enum = static_cast<menu>(menu_item);
        switch (menu_enum)
        {
            case menu::view_schedule:
            {
                if (!view_schedule(read_schedule_from_file(schedule_path)))
                    is_exit = true;
                break;
            }
            case menu::add_new_train_into_file:
            {
                train new_train = get_train_stdin();
                add_new_train_into_file(new_train, schedule_path);
                break;
            }
            case menu::linear_search:
            {
                linear_search_into_file(schedule_path);
                break;
            }
            case menu::linear_sort:
            {
                if (!linear_sort_schedule_in_file(schedule_path))
                    is_exit = true;
                break;
            }
            case menu::quickSort:
            {
                if (!quick_sort_schedule_in_file(schedule_path))
                    is_exit = true;
                break;
            }
            case menu::binary_search:
            {
                std::vector<train> schedule =
                    read_schedule_from_file(schedule_path);
                quick_sort(schedule, 0, schedule.size() - 1);

                if (!is_sorted_ascending(schedule))
                {
                    std::cout << "list of trins isn't sorted." << std::endl;
                    break;
                }

                size_t seat_num = 0;
                std::cout << "Enter number of seats (key): ";
                std::cin >> seat_num;

                train tr = binary_search_schedule(schedule, seat_num);
                if (tr.available_seats_ == seat_num)
                    std::cout << "Found a train:\n" << tr << std::endl;
                else
                    std::cout << "Train not found." << std::endl;

                break;
            }
            case menu::private_task:
            {
                std::vector<train> trains =
                    read_schedule_from_file(schedule_path);
                bool train_exist = ordering_tickets(trains);
                if (!train_exist)
                    std::cout << "\nNo suitable trains.\n" << std::endl;
                break;
            }
            case menu::create_new_file:
            {
                if (!create_new_schedule_file(schedule_path))
                    is_exit = true;
                break;
            }
            case menu::exit:
            {
                is_exit = true;
                break;
            }
            default:
                return EXIT_FAILURE;
        }
    }

    return is_exit ? EXIT_SUCCESS : EXIT_FAILURE;
}
